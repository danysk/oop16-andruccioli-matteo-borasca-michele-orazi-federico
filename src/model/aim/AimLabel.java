package model.aim;
/**
 * 
 * Enumeration that contains labels for the possible aims.
 *
 */
public enum AimLabel {
    /**
     * List of the possible aimLabels.
     * 
     * DCA->DefinedConquerAim
     * UCA->UndefinedConquerAim
     * SA ->StateAim
     * DA ->DestroyAim
     * DGA->DoubleGarrisonAim
     * 
     */
    DCA, UCA, SA, DA, DGA;
}
